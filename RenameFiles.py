'''
RenameFiles
Created By: ComputerCrash0
'''

import os
from time import gmtime,strftime

DATE = strftime("%Y%m%d%H%M%S", gmtime())
path = 'C:\\Test' # Call this path instead. Its common practice
os.chdir(path)


def renameFile(date=DATE): # changed this to keyword args with a default value.
    #for files in os.walk(os.getcwd()):
    for i,file in enumerate(os.listdir()):
        if file.startswith("Test"):
            file_ext = os.path.splitext(file)[1]
            file_name = str(os.path.split(os.getcwd())[1])
            newName = '{}_{}_{}_{}'.format(i, file_name, DATE, file_ext)
            os.rename(file, newName)

renameFile()
